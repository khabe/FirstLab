# Best Gamepad

## The Good

1. Xbox 360 / One / X series
2. Nintendo Switch controller pro
3. Gamecube
4. Megadrive (Genesis)
5. SNES

## The Bad (I don't have T-rex hands)

1. Playstation 3 (the boomerang)
2. Playstation 1
3. Playstation 2
4. Playstation 3 / 4
